package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/discovery"
)

func TestValidateAIClientData(t *testing.T) {
	tests := []struct {
		name     string
		apiV4Url string
		jobID    string
		token    string
		wantErr  bool
	}{
		{"valid data", "http://127.0.0.1:3000/api/v4", "1", "secret", false},
		{"url missing", "", "1", "secret", true},
		{"jobID missing", "http://127.0.0.1:3000/api/v4", "", "secret", true},
		{"token missing", "http://127.0.0.1:3000/api/v4", "1", "", true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testApp := application{
				aiclientData: struct {
					apiV4Url string
					jobID    string
					token    string
				}{tt.apiV4Url, tt.jobID, tt.token},
			}

			gotErr := testApp.validateAIClientData()
			if tt.wantErr {
				require.Error(t, gotErr)
			} else {
				require.NoError(t, gotErr)
			}
		})
	}
}

const internalTestData = "../../internal/testdata/dep_files"

func TestCreateReports(t *testing.T) {
	tests := []struct {
		name                 string
		scanDir              string
		depsByType           map[deps.Type][]*discovery.DependencyFile
		mockCompletionRespFn func(string) string
		expected             map[string]string
	}{
		{
			"single test",
			internalTestData,
			map[deps.Type][]*discovery.DependencyFile{
				deps.PythonPip: {
					{FileName: "requirements.txt", DepType: deps.PythonPip, FoundPath: filepath.Join(internalTestData, "requirements.txt")},
				},
			},
			func(_ string) string {
				return `{
					"response": "uvicorn --- An ASGI web server implementation for Python.\npython-dotenv --- Reads key-value pairs from a .env file and can set them as environment variables.\n\npytest 7.2.0 --- A Python testing framework with features for test automation, sharing fixtures, parallel testing, and detailed info on failing assert statements.\nfastapi 0.104.1 --- A modern, high-performance, Python 3.6+ web framework that uses type hints.\ndetect-secrets 1.4.0 --- A tool to detect secrets like passwords or API keys in code repositories.\nfastapi-health 0.3.0 --- Provides health check endpoints for FastAPI applications.\ntree-sitter 0.20.4 --- An incremental parsing system for programming tools.\nanthropic 0.7.7 --- A Python library for training AI models to be helpful, harmless, and honest."
				}`
			},
			map[string]string{
				"python.json": `{
					"scannerVersion": "1.3.0",
					"fileName": "requirements.txt",
					"libs": [
						{
							"name": "uvicorn",
							"description": "An ASGI web server implementation for Python."
						},
						{
							"name": "python-dotenv",
							"description": "Reads key-value pairs from a .env file and can set them as environment variables."
						},
						{
							"name": "pytest 7.2.0",
							"description": "A Python testing framework with features for test automation, sharing fixtures, parallel testing, and detailed info on failing assert statements."
						},
						{
							"name": "fastapi 0.104.1",
							"description": "A modern, high-performance, Python 3.6+ web framework that uses type hints."
						},
						{
							"name": "detect-secrets 1.4.0",
							"description": "A tool to detect secrets like passwords or API keys in code repositories."
						},
						{
							"name": "fastapi-health 0.3.0",
							"description": "Provides health check endpoints for FastAPI applications."
						},
						{
							"name": "tree-sitter 0.20.4",
							"description": "An incremental parsing system for programming tools."
						},
						{
							"name": "anthropic 0.7.7",
							"description": "A Python library for training AI models to be helpful, harmless, and honest."
						}
					]
				}`,
			},
		},
		{
			"multi type test",
			internalTestData,
			map[deps.Type][]*discovery.DependencyFile{
				deps.PythonPip: {
					{FileName: "requirements.txt", DepType: deps.PythonPip, FoundPath: filepath.Join(internalTestData, "requirements.txt")},
				},
				deps.JavaScript: {
					{FileName: "package.json", DepType: deps.JavaScript, FoundPath: filepath.Join(internalTestData, "package.json")},
				},
				deps.Ruby: {
					{FileName: "Gemfile.lock", DepType: deps.Ruby, FoundPath: filepath.Join(internalTestData, "Gemfile.lock")},
				},
			},
			func(req string) string {
				var resp string

				javascriptRegex, _ := regexp.Compile(`(?i)javascript`)
				pythonRegex, _ := regexp.Compile(`(?i)python`)
				rubyRegex, _ := regexp.Compile(`(?i)ruby`)

				switch {
				case javascriptRegex.MatchString(req):
					resp = `@apollo/client 3.5.10 --- A fully-featured caching GraphQL client.\n@babel/core 7.18.5 --- Babel compiler core.\n@rails/actioncable 7.0.8 --- Integrated WebSockets for Rails.\n@rails/ujs 7.0.8 --- Unobtrusive scripting adapter for Rails.\nvue 2.7.15 --- Reactive, component-oriented view layer for modern web interfaces.`
				case pythonRegex.MatchString(req):
					resp = `uvicorn --- An ASGI web server implementation for Python.\npython-dotenv --- Reads key-value pairs from a .env file and can set them as environment variables.\n\npytest 7.2.0 --- A Python testing framework with features for test automation, sharing fixtures, parallel testing, and detailed info on failing assert statements.\nfastapi 0.104.1 --- A modern, high-performance, Python 3.6+ web framework that uses type hints.\ndetect-secrets 1.4.0 --- A tool to detect secrets like passwords or API keys in code repositories.\nfastapi-health 0.3.0 --- Provides health check endpoints for FastAPI applications.\ntree-sitter 0.20.4 --- An incremental parsing system for programming tools.\nanthropic 0.7.7 --- A Python library for training AI models to be helpful, harmless, and honest.`
				case rubyRegex.MatchString(req):
					resp = `activesupport 7.0.8 --- A toolkit of support libraries and Ruby core extensions extracted from the Rails framework.\nconcurrent-ruby 1.2.0 --- Modern concurrency tools for Ruby. Inspired by Erlang, Clojure, Scala, Haskell, F#, C#, Java, and classic concurrency patterns.\nattr_encrypted 3.2.4 --- Generates attr_accessors that encrypt and decrypt attributes transparently.\nawesome_print 1.9.2 --- Pretty print your Ruby objects with style -- in full color and with proper indentation.\nbcrypt 3.1.18 --- Secure hash algorithm library for Ruby.\nbetter_errors 2.10.1 --- Provides a better error page for Rails and other Rack apps. Includes source code inspection, a live REPL and local/instance variable inspection for all stack frames.\ncapybara 3.39.2 --- Capybara is an integration testing tool for rack based web applications. It simulates how a user would interact with a website.\ndevise 4.9.3 --- Flexible authentication solution for Rails with Warden.\nkaminari 1.2.2 --- A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps.\nrails 7.0.8 --- Full-stack web application framework.`
				}

				return fmt.Sprintf(`{
					"response": "%s"
				}`, resp)
			},
			map[string]string{
				"python.json": `{
					"scannerVersion": "1.3.0",
					"fileName": "requirements.txt",
					"libs": [
						{
							"name": "uvicorn",
							"description": "An ASGI web server implementation for Python."
						},
						{
							"name": "python-dotenv",
							"description": "Reads key-value pairs from a .env file and can set them as environment variables."
						},
						{
							"name": "pytest 7.2.0",
							"description": "A Python testing framework with features for test automation, sharing fixtures, parallel testing, and detailed info on failing assert statements."
						},
						{
							"name": "fastapi 0.104.1",
							"description": "A modern, high-performance, Python 3.6+ web framework that uses type hints."
						},
						{
							"name": "detect-secrets 1.4.0",
							"description": "A tool to detect secrets like passwords or API keys in code repositories."
						},
						{
							"name": "fastapi-health 0.3.0",
							"description": "Provides health check endpoints for FastAPI applications."
						},
						{
							"name": "tree-sitter 0.20.4",
							"description": "An incremental parsing system for programming tools."
						},
						{
							"name": "anthropic 0.7.7",
							"description": "A Python library for training AI models to be helpful, harmless, and honest."
						}
					]
				}`,
				"javascript.json": `{
					"scannerVersion": "1.3.0",
					"fileName": "package.json",
					"libs": [
						{
							"name": "@apollo/client 3.5.10",
							"description": "A fully-featured caching GraphQL client."
						},
						{
							"name": "@babel/core 7.18.5",
							"description": "Babel compiler core."
						},
						{
							"name": "@rails/actioncable 7.0.8",
							"description": "Integrated WebSockets for Rails."
						},
						{
							"name": "@rails/ujs 7.0.8",
							"description": "Unobtrusive scripting adapter for Rails."
						},
						{
							"name": "vue 2.7.15",
							"description": "Reactive, component-oriented view layer for modern web interfaces."
						}
					]
				}`,
				"ruby.json": `{
					"scannerVersion": "1.3.0",
					"fileName": "Gemfile.lock",
					"libs": [
						{
							"name": "activesupport 7.0.8",
							"description": "A toolkit of support libraries and Ruby core extensions extracted from the Rails framework."
						},
						{
							"name": "concurrent-ruby 1.2.0",
							"description": "Modern concurrency tools for Ruby. Inspired by Erlang, Clojure, Scala, Haskell, F#, C#, Java, and classic concurrency patterns."
						},
						{
							"name": "attr_encrypted 3.2.4",
							"description": "Generates attr_accessors that encrypt and decrypt attributes transparently."
						},
						{
							"name": "awesome_print 1.9.2",
							"description": "Pretty print your Ruby objects with style -- in full color and with proper indentation."
						},
						{
							"name": "bcrypt 3.1.18",
							"description": "Secure hash algorithm library for Ruby."
						},
						{
							"name": "better_errors 2.10.1",
							"description": "Provides a better error page for Rails and other Rack apps. Includes source code inspection, a live REPL and local/instance variable inspection for all stack frames."
						},
						{
							"name": "capybara 3.39.2",
							"description": "Capybara is an integration testing tool for rack based web applications. It simulates how a user would interact with a website."
						},
						{
							"name": "devise 4.9.3",
							"description": "Flexible authentication solution for Rails with Warden."
						},
						{
							"name": "kaminari 1.2.2",
							"description": "A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps."
						},
						{
							"name": "rails 7.0.8",
							"description": "Full-stack web application framework."
						}
					]
				}`,
			},
		},
	}

	app := NewApplication()
	reportsDir, err := os.MkdirTemp("", "testReports")
	if err != nil {
		t.Fatalf("Failed to create temporary reports directory: %v", err)
	}
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {
			t.Errorf("Failed to remove temporary test directory: %v", err)
		}
	}(reportsDir)

	for _, tt := range tests {
		server := StartMockServer(t, tt.mockCompletionRespFn)

		app.scanDir = tt.scanDir
		app.reportsDir = reportsDir
		app.aiclientData.apiV4Url = server.URL
		app.aiclientData.jobID = "1234"
		app.aiclientData.token = "abc123"

		t.Run(tt.name, func(t *testing.T) {
			app.createReports(tt.depsByType)

			for depType := range tt.depsByType {
				dstFile := dstFiles[depType]

				reportFile := filepath.Join(reportsDir, "libs", dstFile)
				if _, err := os.Stat(reportFile); os.IsNotExist(err) {
					t.Errorf("Report file was not created: %s", reportFile)
				}

				content, err := os.ReadFile(reportFile)
				if err != nil {
					t.Errorf("Failed to read report file: %v", err)
				}

				var actual map[string]interface{}
				if err := json.Unmarshal(content, &actual); err != nil {
					t.Errorf("Failed to unmarshal report JSON: %v", err)
				}

				var expected map[string]interface{}
				if err := json.Unmarshal([]byte(tt.expected[dstFile]), &expected); err != nil {
					t.Errorf("Failed to unmarshal expected JSON: %v", err)
				}

				// Currently this attribute is present in the output JSON but is not being used. So ignore for now.
				delete(actual, "checksum")

				if !reflect.DeepEqual(actual, expected) {
					t.Errorf("Generated report does not match expected output")
					t.Errorf("Actual: %v", actual)
					t.Errorf("Expected: %v", expected)
				}
			}
		})

		server.Close()
	}
}

func TestFindDependencies(t *testing.T) {
	tests := []struct {
		name     string
		scanDir  string
		deps     []*discovery.DependencyFile
		expected []*discovery.DependencyFile
	}{
		{
			"single test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DirPath: internalTestData, DepType: deps.PythonPip},
			},
		},
		{
			"custom test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "dev.txt", DirPath: internalTestData, DepType: deps.PythonPip, SearchSubdirs: true},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "dev.txt", DirPath: internalTestData + "/dev", DepType: deps.PythonPip, SearchSubdirs: true},
			},
		},
		{
			"multi type test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "package.json", DepType: deps.JavaScript},
				{FileName: "Gemfile.lock", DepType: deps.Ruby},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "package.json", DirPath: internalTestData, DepType: deps.JavaScript},
				{FileName: "Gemfile.lock", DirPath: internalTestData, DepType: deps.Ruby},
			},
		},
		{
			"multi file test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "multi.txt", DepType: deps.PythonPip, SearchSubdirs: true},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "multi.txt", DirPath: internalTestData, DepType: deps.PythonPip, SearchSubdirs: true},
				{FileName: "multi.txt", DirPath: internalTestData + "/dev", DepType: deps.PythonPip, SearchSubdirs: true},
			},
		},
	}

	app := NewApplication()

	for _, tt := range tests {
		app.scanDir = tt.scanDir
		t.Run(tt.name, func(t *testing.T) {
			foundDeps := app.findDependencies(tt.deps)

			require.Greater(t, len(foundDeps), 0)

			for ndx, exp := range tt.expected {
				dep := foundDeps[ndx]
				require.Equal(t, dep.DepType, exp.DepType)
				require.Equal(t, dep.Found, true)
				require.Equal(t, dep.DirPath, internalTestData)
				require.Equal(t, dep.FileName, exp.FileName)
			}
		})
	}
}

func StartMockServer(t *testing.T, respFn func(string) string) *httptest.Server {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/internal/jobs/1234/x_ray/scan" {
			t.Errorf("Expected to request '/internal/jobs/1234/x_ray/scan', got: %s", r.URL.Path)
		}

		body, readErr := io.ReadAll(r.Body)
		if readErr != nil {
			t.Errorf("Failed to read request body: %v", readErr)
			return
		}

		w.WriteHeader(http.StatusOK)

		resp := respFn(string(body))
		_, writeErr := w.Write([]byte(resp))
		if writeErr != nil {
			t.Errorf("Failed to write mocked HTTP response: %v", writeErr)
		}
	}))

	return server
}
