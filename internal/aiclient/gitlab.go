package aiclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/prompt"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/pkg/xlog"
)

type GitLab struct {
	ApiV4Url       string
	JobID          string
	Token          string
	promptType     string
	provider       string
	model          string
	scannerVersion string
}

type GitLabRequest struct {
	Token            string                         `json:"token"`
	PromptComponents []GitLabRequestPromptComponent `json:"prompt_components"`
}

type GitLabRequestPromptComponent struct {
	Type     string                               `json:"type"`
	Payload  GitLabRequestPromptComponentPayload  `json:"payload"`
	Metadata GitLabRequestPromptComponentMetadata `json:"metadata"`
}

type GitLabRequestPromptComponentPayload struct {
	Prompt   string `json:"prompt"`
	Provider string `json:"provider"`
	Model    string `json:"model"`
}

type GitLabRequestPromptComponentMetadata struct {
	ScannerVersion string `json:"scannerVersion"`
}

var (
	maxRetries     = 3
	baseRetryDelay = 3.0
)

type GitLabResponse struct {
	Completion string `json:"response"`
}

type GitLabErrorResponse struct {
	Message string `json:"message"`
}

func NewGitLab(scannerVersion, apiV4Url, jobID, token string) *GitLab {
	return &GitLab{
		ApiV4Url:       apiV4Url,
		JobID:          jobID,
		Token:          token,
		promptType:     "x_ray_package_file_prompt",
		provider:       "anthropic",
		model:          "claude-2.0",
		scannerVersion: scannerVersion,
	}
}

func (gl *GitLab) apiEndpoint() string {
	return fmt.Sprintf("%s/internal/jobs/%s/x_ray/scan", gl.ApiV4Url, gl.JobID)
}

func (gl *GitLab) newGitLabRequest(prompt string) *GitLabRequest {
	return &GitLabRequest{
		Token: gl.Token,
		PromptComponents: []GitLabRequestPromptComponent{
			{
				Type: gl.promptType,
				Payload: GitLabRequestPromptComponentPayload{
					Prompt:   prompt,
					Provider: gl.provider,
					Model:    gl.model,
				},
				Metadata: GitLabRequestPromptComponentMetadata{
					ScannerVersion: gl.scannerVersion,
				},
			},
		},
	}
}

func (gl *GitLab) Completions(prompt string) ([]deps.Dependency, error) {
	data := gl.newGitLabRequest(prompt)
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	var requestCounter int
	var resp *http.Response
	client := &http.Client{}
	for {
		if requestCounter++; requestCounter > maxRetries {
			return nil, fmt.Errorf("Reached maximum retries (%d), giving up", maxRetries)
		}

		req, err := http.NewRequest("POST", gl.apiEndpoint(), bytes.NewReader(jsonData))
		if err != nil {
			return nil, err
		}

		req.Header.Set("Content-Type", "application/json")

		resp, err = client.Do(req)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		if resp.StatusCode == http.StatusServiceUnavailable || resp.StatusCode == http.StatusTooManyRequests {
			resp.Body.Close()
			delay := math.Pow(baseRetryDelay, float64(requestCounter)+rand.Float64())
			duration := time.Duration(delay) * time.Second
			xlog.Warn("Request failed (code %v), will retry in %s", resp.StatusCode, duration)
			time.Sleep(duration)
			continue
		}

		if resp.StatusCode != http.StatusOK {
			var gler GitLabErrorResponse
			err = json.NewDecoder(resp.Body).Decode(&gler)
			if err != nil {
				return nil, fmt.Errorf("Failed to decode response body: %v", err)
			}

			return nil, fmt.Errorf(gler.ErrorMessage(resp.StatusCode))
		}

		break
	}

	var glr GitLabResponse
	err = json.NewDecoder(resp.Body).Decode(&glr)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode response body: %v", err)
	}

	r := parseBody(glr.String())
	return r, nil
}

func (glr *GitLabResponse) String() string {
	return strings.TrimSuffix(glr.Completion, prompt.ResponseClosingTag)
}

func (gler *GitLabErrorResponse) ErrorMessage(statusCode int) string {
	if gler.Message != "" {
		return fmt.Sprintf("Request failed (code %d): %s", statusCode, gler.Message)
	}

	return fmt.Sprintf("Request failed (code %d)", statusCode)
}
