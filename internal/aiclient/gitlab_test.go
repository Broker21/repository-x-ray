package aiclient

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

func Test_apiEndpoint(t *testing.T) {
	gl := NewGitLab("0.0.1", "http://localhost:3000/api/v4", "1", "secret-token")
	want := "http://localhost:3000/api/v4/internal/jobs/1/x_ray/scan"
	got := gl.apiEndpoint()

	require.Equal(t, want, got)
}

func TestGitLabCompletions(t *testing.T) {
	tests := []struct {
		name         string
		status       int
		resp         string
		want         []deps.Dependency
		wantErr      bool
		wantErrMsg   string
		requestCount int
	}{
		{
			"success",
			http.StatusOK,
			`{"response": "rails --- Ruby on Rails description\nkaminari --- Kaminari description</description>"}`,
			[]deps.Dependency{
				{Name: "rails", Description: "Ruby on Rails description"},
				{Name: "kaminari", Description: "Kaminari description"},
			},
			false,
			"",
			1,
		},
		{
			"forbidden",
			http.StatusForbidden,
			`{}`,
			[]deps.Dependency{},
			true,
			"Request failed (code 403)",
			1,
		},
		{
			"response is invalid JSON",
			http.StatusOK,
			`rails --- Ruby on Rails description\nkaminari --- Kaminari description</description>`,
			[]deps.Dependency{},
			true,
			"Failed to decode response body: invalid character 'r' looking for beginning of value",
			1,
		},
		{
			"unauthorized",
			http.StatusUnauthorized,
			`{"message":"401 Unauthorized - GitLab Duo Pro Add-On purchase can't be found"}`,
			[]deps.Dependency{},
			true,
			"Request failed (code 401): 401 Unauthorized - GitLab Duo Pro Add-On purchase can't be found",
			1,
		},
		{
			"toomany",
			http.StatusTooManyRequests,
			`{}`,
			[]deps.Dependency{},
			true,
			"Reached maximum retries (3), giving up",
			3,
		},
	}
	baseRetryDelay = 0

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			requestCounter := 0
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				requestCounter++
				w.WriteHeader(tt.status)
				_, _ = w.Write([]byte(tt.resp))
			}))

			gl := NewGitLab("0.0.1", server.URL, "1", "secret-token")

			got, err := gl.Completions("prompt")
			if tt.wantErr {
				require.Error(t, err)
				// test error message
				require.EqualError(t, err, tt.wantErrMsg)
			} else {
				// reports will be generated
				require.NoError(t, err)
			}
			require.ElementsMatch(t, tt.want, got)
			require.Equal(t, tt.requestCount, requestCounter)
		})
	}
}
