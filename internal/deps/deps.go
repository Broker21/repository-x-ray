package deps

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

type Type int

const (
	Ruby Type = iota
	JavaScript
	Go
	PythonPoetry
	PythonPip
	PythonConda
	PHP
	JavaMaven
	JavaGradle
	KotlinGradle
	CSharp
	CppConanTxt
	CppConanPy
	CppVcpkg
)

type Dependency struct {
	Name        string `json:"name"`
	Version     string `json:"version,omitempty"`
	Description string `json:"description,omitempty"`
}

func (d Dependency) String() string {
	return fmt.Sprintf("Name: %s, Version: %s, Description: %s", d.Name, d.Version, d.Description)
}

func TypeDescription(t Type) string {
	switch t {
	case Ruby:
		return "Ruby gems"
	case JavaScript:
		return "JavaScript modules"
	case Go:
		return "Go packages"
	case PythonPoetry, PythonPip, PythonConda:
		return "Python packages"
	case PHP:
		return "PHP packages"
	case JavaMaven, JavaGradle:
		return "Java dependencies"
	case KotlinGradle:
		return "Kotlin dependencies"
	case CSharp:
		return "C# packages"
	case CppConanTxt, CppConanPy, CppVcpkg:
		return "C/C++ dependencies"
	default:
		return ""
	}
}

func (t Type) String() string {
	return TypeDescription(t)
}

func NewDependency(name, version string) Dependency {
	return Dependency{
		Name:    name,
		Version: version,
	}
}

// NewRubyGem parses a string in the format of "gem_name (version)" and returns a RubyGem struct
func NewRubyGem(raw string) Dependency {
	g := Dependency{}

	g.Name, g.Version, _ = strings.Cut(strings.TrimSpace(raw), " ")

	return g
}

func NewGoPackage(raw string) Dependency {
	g := Dependency{}

	g.Name, g.Version, _ = strings.Cut(strings.TrimSpace(raw), " ")

	return g
}

func NewPythonPoetryPackage(raw string) Dependency {
	d := Dependency{}

	name, rawVer, _ := strings.Cut(strings.TrimSpace(raw), " = ")
	d.Name = name
	if strings.HasPrefix(rawVer, `"`) {
		d.Version = strings.Trim(rawVer, `"`)
		return d
	}

	// A version could be hidden in a nested structure, such as:
	// uvicorn = { extras = ["standard"], version = "^0.20.0" }
	re := regexp.MustCompile(`version \= "(.*)"`)
	matches := re.FindStringSubmatch(rawVer)
	if len(matches) == 2 {
		d.Version = matches[1]
	}

	return d
}

func NewPythonPipPackage(raw string) Dependency {
	d := Dependency{}

	re := regexp.MustCompile(`^([a-z-_]+)\s*[!=~>]{0,1}=*\s*([0-9\.]*)`)
	matches := re.FindStringSubmatch(raw)
	if len(matches) == 3 {
		d.Name = matches[1]
		d.Version = matches[2]
	}

	return d
}

func NewPythonCondaPackage(raw string) Dependency {
	d := Dependency{}

	name, version, hasVersion := strings.Cut(strings.TrimSpace(raw), "=")
	d.Name = name

	if hasVersion {
		d.Version = version
	}

	return d
}

func ParseGradleDependency(raw string) (Dependency, error) {
	d := Dependency{}
	prefix := `(implementation|testImplementation|"implementation"|"testImplementation")`
	parseErr := errors.New("failed to parse gradle dependency")

	// Check if dependency specified using long format
	if strings.Contains(raw, "name:") && strings.Contains(raw, "version:") {
		// Parse dependency in long format
		expr := `%s.+%s\:\s+\'([a-z0-9-.]+)\'`

		// extract dependency name
		re := regexp.MustCompile(fmt.Sprintf(expr, prefix, "name"))
		matches := re.FindStringSubmatch(raw)
		if len(matches) != 3 {
			return d, parseErr
		}
		d.Name = matches[2]

		// extract dependency version
		re = regexp.MustCompile(fmt.Sprintf(expr, prefix, "version"))
		matches = re.FindStringSubmatch(raw)
		if len(matches) == 3 {
			d.Version = matches[2]
		}

		return d, nil
	}

	// Parse dependency in short format
	re := regexp.MustCompile(fmt.Sprintf(`%s[\s+(]["']([a-z0-9-.:]+)["']`, prefix))
	matches := re.FindStringSubmatch(raw)
	if len(matches) != 3 {
		return d, parseErr
	}

	dep := matches[2]
	parts := strings.Split(dep, ":")
	if len(parts) != 3 {
		return d, parseErr
	}

	d.Name = parts[1]
	d.Version = parts[2]

	return d, nil
}

func ParseCppConanTxtDependency(raw string) Dependency {
	d := Dependency{}

	parts := strings.Split(raw, "#")
	raw, _, _ = strings.Cut(strings.TrimSpace(parts[0]), "@")
	parts = strings.Split(raw, "/")
	d.Name = parts[0]
	if len(parts) == 2 {
		d.Version = parts[1]
	}

	return d
}

func ParseCppConanPyDependency(raw string) Dependency {
	// Format: self.requires("zlib/1.2.11")
	_, rawDep, found := strings.Cut(strings.TrimSpace(raw), `self.requires(`)
	if found {
		rawDep = strings.TrimSuffix(rawDep, `)`)
		rawDep = strings.Trim(rawDep, `"`)
		return ParseCppConanTxtDependency(rawDep)
	}

	// Format: "zlib/1.2.11"
	parts := strings.Split(raw, `"`)
	if len(parts) != 3 {
		return Dependency{}
	}

	return ParseCppConanTxtDependency(parts[1])
}
