package scanner

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

func TestScan(t *testing.T) {
	tests := []struct {
		name    string
		path    string
		depType deps.Type
		want    []deps.Dependency
		wantErr bool
	}{
		{
			"with Gemfile.lock",
			"../testdata/dep_files/Gemfile.lock",
			deps.Ruby,
			[]deps.Dependency{
				deps.NewRubyGem("attr_encrypted (~> 3.2.4)!"),
				deps.NewRubyGem("awesome_print"),
				deps.NewRubyGem("bcrypt (~> 3.1, >= 3.1.14)"),
				deps.NewRubyGem("better_errors (~> 2.10.1)"),
				deps.NewRubyGem("capybara (~> 3.39, >= 3.39.2)"),
				deps.NewRubyGem("devise (~> 4.9.3)"),
				deps.NewRubyGem("kaminari (~> 1.2.2)"),
				deps.NewRubyGem("rails (~> 7.0.8)"),
			},
			false,
		},
		{
			"without Gemfile.lock",
			"../internal/testdata/empty/Gemfile.lock",
			deps.Ruby,
			[]deps.Dependency{},
			true,
		},
		{
			"with package.json",
			"../testdata/dep_files/package.json",
			deps.JavaScript,
			[]deps.Dependency{
				deps.NewDependency("@apollo/client", "^3.5.10"),
				deps.NewDependency("@babel/core", "^7.18.5"),
				deps.NewDependency("@rails/actioncable", "7.0.8"),
				deps.NewDependency("@rails/ujs", "7.0.8"),
				deps.NewDependency("vue", "2.7.15"),
				deps.NewDependency("jest", "^28.1.3"),
				deps.NewDependency("sass", "^1.69.0"),
			},
			false,
		},
		{
			"without package.json",
			"../internal/testdata/empty/package.json",
			deps.JavaScript,
			[]deps.Dependency{},
			true,
		},
		{
			"with go.mod",
			"../testdata/dep_files/go.mod",
			deps.Go,
			[]deps.Dependency{
				deps.NewGoPackage("github.com/joho/godotenv v1.5.1"),
				deps.NewGoPackage("github.com/stretchr/testify v1.8.4"),
			},
			false,
		},
		{
			"with go.mod and single package",
			"../testdata/dep_files/go1.mod",
			deps.Go,
			[]deps.Dependency{
				deps.NewGoPackage("github.com/joho/godotenv v1.5.1"),
			},
			false,
		},
		{
			"without go.mod",
			"../internal/testdata/empty/go.mod",
			deps.Go,
			[]deps.Dependency{},
			true,
		},
		{
			"with pyproject.toml",
			"../testdata/dep_files/pyproject.toml",
			deps.PythonPoetry,
			[]deps.Dependency{
				deps.NewPythonPoetryPackage(`python = "~3.9"`),
				deps.NewPythonPoetryPackage(`uvicorn = { extras = ["standard"], version = "^0.20.0" }`),
				deps.NewPythonPoetryPackage(`python-dotenv = "^0.21.0"`),
				deps.NewPythonPoetryPackage(`pytest = "^7.2.0"`),
				deps.NewPythonPoetryPackage(`flake8 = "^6.0.0"`),
			},
			false,
		},
		{
			"without pyproject.toml",
			"../internal/testdata/empty/pyproject.toml",
			deps.PythonPoetry,
			[]deps.Dependency{},
			true,
		},
		{
			"with requirements.txt",
			"../testdata/dep_files/requirements.txt",
			deps.PythonPip,
			[]deps.Dependency{
				{Name: "fastapi", Version: "0.104.1"},
				{Name: "detect-secrets", Version: "1.4.0"},
				{Name: "fastapi-health", Version: "0.3.0"},
				{Name: "tree-sitter", Version: "0.20.4"},
				{Name: "anthropic", Version: "0.7.7"},
				{Name: "uvicorn"},
				{Name: "python-dotenv"},
				{Name: "pytest", Version: "7.2.0"},
				{Name: "babel", Version: "2.14.0"},
				{Name: "certifi", Version: "2023.11.17"},
				{Name: "tensorflow", Version: "2.3.1"},
				{Name: "alabaster", Version: "0.7.16"},
			},
			false,
		},
		{
			"without requirements.txt",
			"../internal/testdata/empty/requirments.txt",
			deps.PythonPip,
			[]deps.Dependency{},
			true,
		},
		{
			"with environment.yml",
			"../testdata/dep_files/environment.yml",
			deps.PythonConda,
			[]deps.Dependency{
				deps.NewPythonCondaPackage(`python=3.9`),
				deps.NewPythonCondaPackage(`bokeh=2.4.2`),
				deps.NewPythonCondaPackage(`conda-forge::numpy=1.21.*`),
				deps.NewPythonCondaPackage(`nodejs=16.13.*`),
				deps.NewPythonCondaPackage(`flask`),
				deps.NewPythonCondaPackage(`fastapi=0.104.1`),
				deps.NewPythonCondaPackage(`pip`),
			},
			false,
		},
		{
			"without environment.yml",
			"../internal/testdata/empty/environment.yml",
			deps.PythonConda,
			[]deps.Dependency{},
			true,
		},
		{
			"with composer.json",
			"../testdata/dep_files/composer.json",
			deps.PHP,
			[]deps.Dependency{
				deps.NewDependency("php", ">=8.1"),
				deps.NewDependency("graphp/graphviz", "^0.2.2"),
				deps.NewDependency("symfony/console", "^6.0 || ^5.0 || ^4.0"),
				deps.NewDependency("phpunit/phpunit", "~9.5"),
			},
			false,
		},
		{
			"without composer.json",
			"../internal/testdata/empty/composer.json",
			deps.PHP,
			[]deps.Dependency{},
			true,
		},
		{
			"with pom.xml",
			"../testdata/dep_files/pom.xml",
			deps.JavaMaven,
			[]deps.Dependency{
				deps.NewDependency("spring", "4.6.4"),
				deps.NewDependency("hibernate", "3.5.4-Final"),
				deps.NewDependency("gson-parent", "2.10.1"),
				deps.NewDependency("junit", "2.0.10"),
			},
			false,
		},
		{
			"without pom.xml",
			"../internal/testdata/empty/pom.xml",
			deps.JavaMaven,
			[]deps.Dependency{},
			true,
		},
		{
			"with build.gradle",
			"../testdata/dep_files/build.gradle",
			deps.JavaGradle,
			[]deps.Dependency{
				deps.NewDependency("example", "1.0"),
				deps.NewDependency("junit-jupiter", "5.9.1"),
				deps.NewDependency("junit", "3.3.0"),
				deps.NewDependency("test-support", "1.3"),
			},
			false,
		},
		{
			"without build.gradle",
			"../internal/testdata/empty/build.gradle",
			deps.JavaGradle,
			[]deps.Dependency{},
			true,
		},
		{
			"with build.gradle.kts",
			"../testdata/dep_files/build.gradle.kts",
			deps.KotlinGradle,
			[]deps.Dependency{
				deps.NewDependency("example", "1.0"),
				deps.NewDependency("junit-jupiter", "5.9.1"),
				deps.NewDependency("junit", "3.3.0"),
				deps.NewDependency("test-support", "1.3"),
				deps.NewDependency("example-string-literal", "1.0"),
				deps.NewDependency("example-test-string-literal", "1.0"),
			},
			false,
		},
		{
			"without build.gradle.kts",
			"../internal/testdata/empty/build.gradle.kts",
			deps.KotlinGradle,
			[]deps.Dependency{},
			true,
		},
		{
			"with *.csproj",
			"../testdata/dep_files/ConsoleApp1/ConsoleApp1.csproj",
			deps.CSharp,
			[]deps.Dependency{
				deps.NewDependency("Newtonsoft.Json", "13.0.3"),
			},
			false,
		},
		{
			"without *.csproj",
			"../internal/testdata/empty/ConsoleApp1.csproj",
			deps.CSharp,
			[]deps.Dependency{},
			true,
		},
		{
			"with conanfile.txt",
			"../testdata/dep_files/conanfile.txt",
			deps.CppConanTxt,
			[]deps.Dependency{
				deps.NewDependency("boost", "1.76.0"),
				deps.NewDependency("opencv", "4.6.0"),
				deps.NewDependency("fmt", "8.0.1"),
				deps.NewDependency("zlib", "1.2.11"),
				deps.NewDependency("openssl", "1.1.1l"),
				deps.NewDependency("sqlite3", "3.37.0"),
				deps.NewDependency("gtest", "1.11.0"),
			},
			false,
		},
		{
			"without conanfile.txt",
			"../internal/testdata/empty/conanfile.txt",
			deps.CppConanTxt,
			[]deps.Dependency{},
			true,
		},
		{
			"with conanfile.py",
			"../testdata/dep_files/conanfile.py",
			deps.CppConanPy,
			[]deps.Dependency{
				deps.NewDependency("boost", "1.76.0"),
				deps.NewDependency("opencv", "4.6.0"),
				deps.NewDependency("poco", "1.11.0"),
				deps.NewDependency("gtest", "1.11.0"),
				deps.NewDependency("catch2", "3.0.0"),
				deps.NewDependency("zlib", "1.2.11"),
				deps.NewDependency("eigen", "3.3.9"),
				deps.NewDependency("base64", "0.4.0"),
			},
			false,
		},
		{
			"without conanfile.py",
			"../internal/testdata/empty/conanfile.py",
			deps.CppConanPy,
			[]deps.Dependency{},
			true,
		},
		{
			"with vcpkg.json",
			"../testdata/dep_files/vcpkg.json",
			deps.CppVcpkg,
			[]deps.Dependency{
				deps.NewDependency("boost", "1.76.0"),
				deps.NewDependency("opencv", ""),
				deps.NewDependency("fmt", "8.0.1"),
				deps.NewDependency("poco", ""),
				deps.NewDependency("gtest", "1.11.0"),
				deps.NewDependency("catch2", ""),
				deps.NewDependency("sqlite3", "3.37.0"),
			},
			false,
		},
		{
			"without vcpkg.json",
			"../internal/testdata/empty/vcpkg.json",
			deps.CppVcpkg,
			[]deps.Dependency{},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Scan(tt.path, tt.depType)

			require.ElementsMatch(t, tt.want, got)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}
