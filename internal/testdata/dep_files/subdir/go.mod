module gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray

go 1.21

require (
	golang.org/x/exp v0.0.0-20240213143201-ec583247a57a
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/mod v0.15.0
)
