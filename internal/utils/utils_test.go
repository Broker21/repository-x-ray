package utils

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEachBatch(t *testing.T) {
	elems := []string{"1", "2", "3", "4", "5", "6", "7"}

	want := []string{
		"1: 1, 2, 3",
		"2: 4, 5, 6",
		"3: 7",
	}

	got := []string{}

	EachBatch(elems, 3, func(batch []string, totalBatches int, currentBatch int) {
		got = append(got, fmt.Sprintf("%d: %s", currentBatch, strings.Join(batch, ", ")))
	})

	require.Equal(t, want, got)
}
